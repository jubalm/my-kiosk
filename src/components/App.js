import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'
import CustomerAuth from '../views/CustomerAuth'
import Customer from '../views/Customer'

import './style.css';

class App extends Component {
  state = {
    online: true
  }

  componentDidMount() {
    window.addEventListener('offline', () => {
      this.setState({ online: false })
    })
    window.addEventListener('online', () => {
      this.setState({ online: true })
    })
  }

  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route exact path="/" component={CustomerAuth}></Route>
          <AuthRoute path="/customer" component={Customer}></AuthRoute>
        </Switch>
        {
          !this.state.online && <div className="system system--warn">Offline</div>
        }
      </React.Fragment>
    );
  }
}

export const Auth = {
  token: localStorage.getItem('mk-app-data') ? JSON.parse(localStorage.getItem('mk-app-data')) : null
}

const AuthRoute = ({ component: Component, ...allprops }) => (
  <Route {...allprops} render={ props => (
    Auth.token ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/',
        state: { from: props.location }
      }}/>
    )
  )} />
)

export default App
