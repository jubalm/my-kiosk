import React from 'react'

const Button = ({ hide, type, onClick, children, disabled=false }) => {
  const handleClick = () => { onClick && onClick() }

  if (hide) return null

  return (
    <div className={`button button--${type}`}>
      <button onClick={handleClick} disabled={disabled}>{children}</button>
    </div>
  )
}

export default Button
