import React from 'react'
import { withRouter } from 'react-router'

const Header =({title, subtitle}) => {
  return (
    <div className="app-header">
      <div className="app-header__title">{title}</div>
      <div className="app-header__subtitle">{subtitle}</div>
    </div>

  )
}
export default withRouter(Header)
