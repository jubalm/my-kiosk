import React from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

const Tender = ({ type, allUsers, onChange = null }) => {
  if (allUsers.error) {
    return <div>There was an error loading users!</div>
  }

  if (allUsers.loading) {
    return <div>Loading&hellip;</div>
  }

  if (type==='select') {
    const lastTenderId = JSON.parse(localStorage.getItem('mk-app-data')).lastTenderId
    return (
      <select onChange={onChange} defaultValue={lastTenderId}>
        <option value="">Select tender</option>
      {
        allUsers.allUsers
          .filter( user => !user.access )
          .map( user => (
            <option key={user.id} value={user.id}>{user.name}</option>
          ))
      }
      </select>
    )
  }

  return <div>test</div>
}

const USERS_QUERY = gql`
  query {
    allUsers {
      id
      name
      access
    }
  }
`

export default graphql(USERS_QUERY, { name: 'allUsers' })(Tender)
