import React, { Component } from 'react';

import ReservationsList from './ReservationsList'

import FixedMenu from './FixedMenu'
import Header from './Header'
import Button from './Button'

class NowServing extends Component {

  state = {
    filterBy: 'customers',
    title: 'Now Serving',
    subtitle: ''
  }

  _customerPage = ({ customer }) => {
    const jumpPath = customer.sales ? `/cart` : ``
    this.props.history.push(`/customer/${customer.id}${jumpPath}`)
  }

  _gotoAvailability = () => {
    this.setState({
      filterBy: 'available',
      title: 'New Customer',
      subtitle: 'Give the customer a queue number'
    })
  }

  _gotoNowServing = () => {
    this.setState({
      filterBy: 'customers',
      title: 'Now Serving',
      subtitle: ''
    })
  }

  _gotoAccount = () => {
    this.props.history.push('/')
  }

  render() {
    return (
      <React.Fragment>
        <Header
          title={this.state.title}
          subtitle={this.state.subtitle}
        />
        <div className="app-body">
          <ReservationsList {...this.props}
            filterBy={this.state.filterBy}
          />
          <Button
            hide={this.state.filterBy === 'available'}
            onClick={this._gotoAvailability}>
            + New Customer
          </Button>
        </div>
        <FixedMenu>
          <Button
            type="solid"
            hide={this.state.filterBy === 'available'}
            onClick={this._gotoAccount}>
            Back <span>Account</span>
          </Button>
          <Button
            type="solid"
            hide={this.state.filterBy === 'customers'}
            onClick={this._gotoNowServing}>
            Back <span>Now Serving</span>
          </Button>
        </FixedMenu>
      </React.Fragment>
    )
  }
}

export default NowServing
