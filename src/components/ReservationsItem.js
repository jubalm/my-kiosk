import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

import './style.css'

class Reservation extends Component {

  _onClick = () => {
    if (this.props.disabled || !this.props.onClick) return
    this.props.onClick()
  }

  render() {
    const { customer, number } = this.props
    const noSale = !customer || !customer.sales.length
    const classNames = `reservation ${this.props.disabled && `reservation--disabled`}`
    const pendingSaleCount = customer && customer.sales.filter(sale => sale.completedAt === null).length

    return (
      <div className={classNames}
        onClick={this._onClick}
      >
        <div className="reservation__details">
          <div className="reservation__number">{number}</div>
          <div className="reservation__name">
            {
              (customer)
                ? (noSale) ? <em>{customer.name}</em> : customer.name
                : null
            }
          </div>
          {
            pendingSaleCount ? <div className="reservation__pending">{ pendingSaleCount }</div> : ``
          }
        </div>
      </div>
    )
  }
}

const CREATECUSTOMER_MUTATIION = gql`
  mutation createCustomer($name: String!, $reservationId: ID!) {
    customer: createCustomer(name:$name, reservationId:$reservationId) {
      id
      reservation {
        id
      }
    }
  }
`

// export default Reservation
export default graphql(CREATECUSTOMER_MUTATIION, { name: 'createCustomer' })(Reservation)
