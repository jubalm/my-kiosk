import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import '../queries.js'

import { RESERVATIONS_QUERY } from '../views/Customer'
import ReservationsItem from './ReservationsItem'

class ReservationsList extends Component {
  _manageReservation = (reservation) => {
    const { history } = this.props

    if (reservation.customer) {
      if (reservation.customer.sales.length) {
        return history.push(`/customer/${reservation.customer.id}/cart`)
      }

      return history.push(`/customer/${reservation.customer.id}`)
    }

    this._createCustomer(reservation)
  }

  _createCustomer = async (reservation) => {
    const { createCustomer, history } = this.props

    try {
      const {data: {createCustomer: createdCustomer}} = await createCustomer({
        variables: {
          reservationId: reservation.id,
          name: "Reserved"
        },

        update: (store, {data: {createCustomer: customer}}) => {
          const data = store.readQuery({ query: RESERVATIONS_QUERY })

          data.allReservations.reduce((a, reservation) => {
            if (reservation.id === customer.reservation.id) {
              reservation.customer = customer
            }
            a.push(reservation)
            return a
          }, [])

          store.writeQuery({ query: RESERVATIONS_QUERY, data })
        }
      })

      history.push(`/customer/${createdCustomer.id}`)

    } catch(e) {
      console.log(e)
    }
  }


  render() {
    const { allReservations:query, filterBy } = this.props

    const filters = {
      'customers': (reservation => reservation.customer),
      'available': (reservation => !reservation.customer)
    }

    if (query.error) return (<p><small>There was an error!</small></p>)
    if (query.loading) return (<p><small>Fetching data...</small></p>)

    if (!query.allReservations.filter(filters[filterBy]).length) {
      return <div>No slots available</div>
    }

    return (
      <div className="reservations">
        {
          query.allReservations
            .filter(filters[filterBy])
            .map((reservation, i) => (
              <ReservationsItem key={i} {...reservation}
                onClick={() => this._manageReservation(reservation)}
              />
            ))
        }
      </div>
    )
  }
}

const CREATECUSTOMER_MUTATION = gql`
  mutation createCustomer($reservationId: ID!, $name: String!) {
    createCustomer(
      reservationId: $reservationId,
      name: $name
    ) {
      id
      name
      sales {
        id
        completedAt
        sellable {
          id
          name
        }
      }
      reservation {
        id
      }
    }
  }
`

export default graphql(CREATECUSTOMER_MUTATION, {
  name: 'createCustomer'
})(ReservationsList)
