import React from 'react'
import moment from 'moment'

const Sale = ({sale, onClick}) => {
  return (
    <div className="sale sale__summary" onClick={onClick && onClick}>
      <div>
        <div>{sale.sellable.name}</div>
        {
          sale.completedAt
            ? <small>served</small>
            : <small className="pending">{moment(sale.createdAt).fromNow()}</small>
        }
      </div>
      <div>{sale.quantity}</div>
      <div>{sale.quantity * sale.sellable.price}</div>
      <div className="arrow left"></div>
    </div>
  )
}

export default Sale
