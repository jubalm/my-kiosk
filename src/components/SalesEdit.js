import React, { Component } from 'react'
import moment from 'moment'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'

import FixedMenu from './FixedMenu'
import Button from './Button'
import { CUSTOMER_QUERY } from '../views/CustomerId'

class SalesEdit extends Component {
  state = {
    saleId: null,
    customerId: null,
  }

  _goBack = () => {
    const { history, queryCustomer: { Customer } } = this.props
    history.push(`/customer/${Customer.id}/cart`)
  }

  _markAsServed = async () => {
    const { match, markAsServed } = this.props
    const { saleId, customerId } = this.state

    await markAsServed({
      variables: {
        saleId: saleId,
        completedAt: moment(new Date()).format()
      },
      update: (store, {data: {updateSale}}) => {
        const data = store.readQuery({
          query: CUSTOMER_QUERY,
          variables: { customerId: customerId }
        })

        data.Customer.sales.find(sale => sale.id === match.params.saleId).completedAt = updateSale.completedAt

        store.writeQuery({ query: CUSTOMER_QUERY, data })
      }
    })
  }

  componentWillMount() {
    const { match, queryCustomer: { Customer } } = this.props

    this.setState({
      saleId: match.params.saleId,
      customerId: Customer.id
    })
  }

  render() {
    const { match, queryCustomer } = this.props

    if (queryCustomer.error) {
      return <div>Something went wrong...</div>
    }

    if (queryCustomer.loading) {
      return <div>Loading...</div>
    }

    const sale = queryCustomer.Customer.sales.find(sale => sale.id === match.params.saleId)

    return (
      <React.Fragment>
        <div className="app-body">
          <div className="details">
            <div>{sale.sellable.name} - Php{sale.sellable.price}</div>
            <div className="sale__detail">
              <span>Selling price</span>
              <div>{sale.sellable.price}</div>
            </div>
            <div className="sale__detail">
              <span>Ordered on</span>
              <div>{moment(sale.createdAt).format('MMMM DD, YYYY h:mm A')}</div>
            </div>
            <div className="sale__detail">
              <span>Tender</span>
              <div>{sale.tender.name}</div>
            </div>
            <div className="sale__detail">
              <span>Cashier</span>
              <div>{sale.cashier.name}</div>
            </div>
            <div className="sale__detail">
              <span>Served At</span>
              {
                sale.completedAt
                  ? <div>{moment(sale.completedAt).format('MMMM DD, YYYY h:mm A')}</div>
                  : <div>Pending</div>
              }
            </div>
          </div>
          <Button type="ok"
            onClick={this._markAsServed}>
            Mark as served
          </Button>
          <Button type="warn">Cancel Order</Button>
        </div>
        <FixedMenu>
          <Button
            type="solid"
            onClick={this._goBack}>
            Back <span>New Customer</span>
          </Button>
        </FixedMenu>
      </React.Fragment>
    )
  }
}

const MARKSAVED_MUTATION = gql`
  mutation markAsServed($saleId:ID!, $completedAt:DateTime!) {
    updateSale(id: $saleId, completedAt:$completedAt) {
      completedAt
    }
  }
`

export default compose(
  graphql(MARKSAVED_MUTATION, { name: 'markAsServed'})
)(SalesEdit)
