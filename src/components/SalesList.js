import React, { Component } from 'react'

import FixedMenu from './FixedMenu'
import Button from './Button'
import Sale from './Sale'

class SalesList extends Component {
  _newOrder = () => {
    const { history, queryCustomer: { Customer }} = this.props
    history.push(`/customer/${Customer.id}/shop`)
  }

  _goBack = () => {
    const { history } = this.props
    history.push(`/customer`)
  }

  _editSale = (saleId) => {
    const { location, history } = this.props
    history.push(`${location.pathname}/${saleId}`)
  }

  _checkout = () => {
    const { history, queryCustomer: { Customer } } = this.props
    history.push(`/customer/${Customer.id}/checkout`)
  }

  render() {
    const { queryCustomer } = this.props

    if (queryCustomer.error) {
      return <div>Error</div>
    }

    if (queryCustomer.loading) {
      return <div>Loading...</div>
    }

    const sales = queryCustomer.Customer.sales

    return (
      <div className="app-body">
        <div className="sales">
        {
          sales.length
            ? (
              <div className="sale sale__summary sale__summary--label">
                <div>item</div>
                <div>quantity</div>
                <div>php</div>
              </div>
            )
            : null
        }
        {
          sales.map( sale => (
            <Sale key={sale.id} sale={sale} onClick={() => this._editSale(sale.id)} />
          ))
        }
        </div>
        <Button onClick={this._newOrder}>+ Add new order</Button>
        <Button type="warn" onClick={this._checkout}>+ Checkout</Button>
        <FixedMenu>
          <Button type="solid" onClick={this._goBack}>
            Back <span>Now Serving</span>
          </Button>
        </FixedMenu>
      </div>
    )
  }
}

export default SalesList
