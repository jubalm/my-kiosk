import React, { Component } from 'react';

class ShopList extends Component {
  render() {
    const {
      allSellables,
      history,
      match
    } = this.props

    if (allSellables.error) {
      return <div className="app-body"><small>Error!</small></div>
    }

    if (allSellables.loading) {
      return <div className="app-body"><small>Loading products...</small></div>
    }

    return (
      <React.Fragment>
        <div className="app-body">
          <div className="sellables">
            {
              allSellables.allSellables.map((sellable, i) => (
                <div className="sellable" key={sellable.id}
                  onClick={() => history.push(`${match.url}/${sellable.id}`)}>
                  <div>{sellable.name}</div>
                  <div>{sellable.price}</div>
                </div>
              ))
            }
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default ShopList
