import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import { matchPath } from 'react-router'

import ListTender from './ListTender'
import Button from './Button'
import { CUSTOMER_QUERY } from '../views/CustomerId'

class ShopListItem extends Component {
  state = {
    quantity: 1,
    sellableId: null,
    disabled: false
  }

  _createSale = async () => {
    const { history, mutationCreateSale } = this.props
    const { customerId, sellableId, quantity, cashierId, tenderId } = this.state

    this.setState({ disabled: true })

    await mutationCreateSale({
      variables: {
        saleCustomerId: customerId,
        sellableId: sellableId,
        quantity: quantity,
        cashierId: cashierId,
        tenderId: tenderId
      },
      update: (store, {data: {createSale}}) => {
        const data = store.readQuery({
          query: CUSTOMER_QUERY,
          variables: { customerId: customerId }
        })

        data.Customer.sales.push(createSale)

        store.writeQuery({ query: CUSTOMER_QUERY, data })
      }
    })

    this.setState({ disabled: false })
    history.push(`/customer/${customerId}/cart`)
  }

  _addQuantity = () => {

    this.setState({
      quantity: this.state.quantity + 1
    })
  }

  _minusQuantity = () => {
    if (this.state.quantity === 1) return
    this.setState({quantity: this.state.quantity - 1})
  }

  _getAmountDue = () => {
    const { match, allSellables: { allSellables } } = this.props
    const price = allSellables && allSellables.find( sellable => sellable.id === match.params.itemId).price
    return price ? this.state.quantity * price : 'calculating...'
  }

  _updateTender = (e) => {
    const tenderId = e.target.value
    this.setState({
      tenderId: tenderId
    })
    const store = JSON.parse(localStorage.getItem('mk-app-data')) || {}
    const data = Object.assign(store, { lastTenderId: tenderId })
    localStorage.setItem('mk-app-data', JSON.stringify(data))
  }

  componentDidMount() {
    const { location } = this.props
    const { params } = matchPath(location.pathname, { path: '/customer/:customerId/shop/:itemId'})
    this.setState({
      customerId: params.customerId,
      sellableId: params.itemId,
      cashierId: JSON.parse(localStorage.getItem('mk-app-data')).authId,
      tenderId: JSON.parse(localStorage.getItem('mk-app-data')).lastTenderId || null
    })
  }

  render() {
    const { match, allSellables, Customer } = this.props

    if (allSellables.error || Customer.error) {
      return <div className="app-body">Something went wrong...</div>
    }

    if (allSellables.loading || Customer.loading) {
      return <div className="app-body">Loading...</div>
    }

    const product = allSellables.allSellables.find( sellable => (
      sellable.id === match.params.itemId)
    )

    const {
      tenderId,
      cashierId,
      quantity,
      disabled
    } = this.state

    return (
      <div className="app-body">
        <div className="sale">
          <div className="sale__name">{product.name} - Php{product.price}</div>
          <div className="sale__detail sale__detail--enum">
            <InputCount label="Quantity"
              value={quantity}
              onAdd={this._addQuantity}
              onMinus={this._minusQuantity}
            />
          </div>
          <div className="sale__detail">
            <span>Amount Due</span>
            <div>{this._getAmountDue()}</div>
          </div>
          <div className="sale__detail">
            <span>Tender</span>
            <ListTender type="select" onChange={this._updateTender}/>
          </div>
        </div>
        <Button type="confirm"
          disabled={disabled || [tenderId, cashierId].some(e => !e)}
          onClick={this._createSale}>
          + Add to order
        </Button>
    </div>
  )
  }
}

class InputCount extends Component {
  componentWillMount() {
    this.setState({ value: this.props.value })
  }

  render() {
    const {onAdd, onMinus, value} = this.props
    return (
      <label className="input-count">
        <span>Quantity</span>
        <div className="input-count__field">{value}</div>
        <div className="input-count__control">
          <i onClick={onMinus}>-</i>
          <i onClick={onAdd}>+</i>
        </div>
      </label>
    )
  }
}

const CREATESALE_MUTATION = gql`
  mutation createSale(
    $saleCustomerId: ID!,
    $sellableId: ID!,
    $quantity:Float!,
    $cashierId: ID!
    $tenderId: ID!
  ) {
    createSale(
      customerId:$saleCustomerId,
      sellableId:$sellableId,
      quantity:$quantity,
      cashierId:$cashierId,
      tenderId:$tenderId
    ) {
      id
      quantity
      updatedAt
      createdAt
      tender {
        id
        name
      }
      cashier {
        id
        name
      }
      completedAt
      sellable {
        id
        price
      }
    }
  }
`

export default graphql(CREATESALE_MUTATION, {
  name: 'mutationCreateSale'
})(ShopListItem)

