const fromEvent = require('graphcool-lib')
const bcrypt = require('bcryptjs')

const SALT_ROUNDS = 10

module.exports = (event) => {
  const lib = fromEvent(event)
  const api = lib.api('simple/v1')
  const { username, password } = event.data

  const { userExist } = await api.request(`{userExist: User(username: ${username}){id}}`)

  if (userExist) {
    return { error: "User already exists" }
  }

  return { data: { id: '123', token: `${username}${password}` } }
}

async function getUser(api: GraphQLClient, email: string): Promise<{ User }> {
  const query = `
    query getUser($email: String!) {
      User(email: $email) {
        id
      }
    }
  `

  const variables = {
    email,
  }

  return api.request<{ User }>(query, variables)
}

async function createGraphcoolUser(api: GraphQLClient, email: string, password: string): Promise<string> {
  const mutation = `
    mutation createGraphcoolUser($email: String!, $password: String!) {
      createUser(
        email: $email,
        password: $password
      ) {
        id
      }
    }
  `

  const variables = {
    email,
    password: password,
  }

  return api.request<{ createUser: User }>(mutation, variables)
    .then(r => r.createUser.id)
}
