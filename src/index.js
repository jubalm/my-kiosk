import React from 'react'
import registerServiceWorker from './registerServiceWorker'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { ApolloClient } from 'apollo-client'
import { ApolloProvider } from 'react-apollo'
import { split } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { WebSocketLink } from 'apollo-link-ws'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { getMainDefinition } from 'apollo-utilities'

import App from './components/App'

import './index.css'

const authToken = localStorage.getItem('mk-app-data') ? JSON.parse(localStorage.getItem('mk-app-data')).authToken : null
const cache = new InMemoryCache({
  dataIdFromObject: o => o.id
})
const httpLink = new HttpLink({ uri: process.env.REACT_APP_GRAPHCOOL_API })
const authLink = setContext((_, { headers }) => {
  return {
    headers: {
      ...headers,
      Authorization: authToken ? `Bearer ${authToken}` : null,
    }
  }
})
const subsClient = new SubscriptionClient(process.env.REACT_APP_GRAPHCOOL_WSS, { reconnect: true })
const wsLink = new WebSocketLink(subsClient)
const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  },
  wsLink,
  authLink.concat(httpLink)
)
export const client = new ApolloClient({
  //link: link,
  link: authLink.concat(httpLink),
  cache
})
const rootEl = document.getElementById('root')

ReactDOM.render(
  <BrowserRouter>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </BrowserRouter>,
  rootEl
)

// hot module replacement
if (module.hot) {
  module.hot.accept('./components/App', () => {
    const NextApp = require('./components/App').default;
    ReactDOM.render(
      <BrowserRouter>
        <ApolloProvider>
          <NextApp />
        </ApolloProvider>
      </BrowserRouter>,
      rootEl
    );
  });
}

// service worker
registerServiceWorker();
