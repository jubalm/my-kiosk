import gql from 'graphql-tag'

export const CustomerFragment = gql`
  fragment ReservationCustomer on Customer {
    id
    name
    sales {
      id
      completedAt
      sellable {
        id
        name
      }
    }
  }
`

export const RESERVATIONS_QUERY = gql`
  query {
    allReservations {
      id
      number
      customer {
        ...ReservatinCustomer
      }
    }
  }
  ${CustomerFragment}
`

export const CREATECUSTOMER_MUTATION = gql`
  mutation createCustomer($reservationId: ID!, $name: String!) {
    createCustomer(
      reservationId: $reservationId,
      name: $name
    ) {
      ...ReservatinCustomer
      reservation {
        id
      }
    }
  }
  ${CustomerFragment}
`

