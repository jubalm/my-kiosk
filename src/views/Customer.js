import React, { Component } from 'react';
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import { Route } from 'react-router-dom'

import NowServing from '../components/NowServing'
import CustomerId from './CustomerId'

class CustomerPage extends Component {
  state = {
    addCustomer: false
  }

  render() {
    const { match, allReservations } = this.props

    return (
      <React.Fragment>
        <Route exact path={`${match.url}/`} render={ props => (
          <NowServing {...props} allReservations={allReservations}/>
        )}/>
        <Route path={`${match.url}/:customerId`} render={ props => (
          <CustomerId {...props}/>
        )}/>
    </React.Fragment>
    );
  }
}

export const RESERVATIONS_QUERY = gql`
  query {
    allReservations {
      id
      number
      customer {
        id
        name
        sales {
          id
          completedAt
          sellable {
            id
            name
            price
          }
        }
      }
    }
  }
`

export default graphql(RESERVATIONS_QUERY, { name: 'allReservations' })(CustomerPage)
