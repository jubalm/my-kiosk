import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import Button from '../components/Button'
import { Auth } from '../components/App'

const decode = (token) => {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(window.atob(base64));
}

class CustomerAuth extends Component {
  state = {
    authenticated: false,
    pending: false
  }

  componentDidMount() {
    const token = localStorage.getItem('mk-app-data')
      ? JSON.parse(localStorage.getItem('mk-app-data')).authToken
      : null

    // signout if token is expired
    if (token && decode(token).exp < Date.now() / 1000) {
      this._signoutUser()
      return
    }

    this.setState({
      authenticated: token ? true : false,
      authMessage: !token ? 'Please sign in' : ''
    })
  }

  render() {
    const { pending, authenticated, authMessage } = this.state

    return (
      <div className="app-body">
        <SigninForm show={!authenticated}
          pending={pending}
          authMessage={authMessage}
          onSignin={this._signinUser}
        />
        <Dashboard {...this.props}
          show={authenticated}
          onSignout={this._signoutUser}
        />
      </div>
    )
  }

  _signinUser = async (username, password) => {
    try {
      this.setState({
        pending: true,
        authMessage: "Signing in..."
      })

      const { data: { signinUser: user }} = await this.props.signinUser({
        variables: {
          username: username,
          password: password
        }
      })

      if (user && user.token) {
        const auth = {
          authToken: user.token,
          authId: user.id,
          authName: user.name,
        }

        await localStorage.setItem('mk-app-data', JSON.stringify(auth))

        Auth.token = user.token

        this.setState({
          pending: false,
          authenticated: true,
          authMessage: `hello ${user.name}`
        })
      }
    } catch(e) {
      const message = e.message.replace('GraphQL error: function execution error: ', '')
      this.setState({
        authMessage: message,
        pending: false
      })
    }
  }

  _signoutUser = async () => {
    await localStorage.removeItem('mk-app-data')

    this.setState({
      authenticated: false,
      authMessage: 'Please sign in'
    })
  }

}

class SigninForm extends Component {
  state = {
    username: "",
    password: ""
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.onSignin(this.state.username, this.state.password)
  }

  render() {
    if (!this.props.show) return null

    const pending = (!this.state.username && !this.state.password)
      || this.props.pending

    return (
      <React.Fragment>
        <p>&nbsp;</p>
        <h1>Login</h1>
        <form className="form-signin" onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="username"
            onChange={(e) => this.setState({ username: e.target.value })}
          />
          <input
            type="password"
            placeholder="password"
            onChange={(e) => this.setState({ password: e.target.value })}
          />
          <button
            disabled={pending}
            type="submit">
            Login
          </button>
        </form>

        <p><small>{this.props.authMessage}</small></p>
      </React.Fragment>
    )
  }
}

const Dashboard = ({ show, onSignout, history }) => {
  if (!show) return null
  const name = JSON.parse(localStorage.getItem('mk-app-data')).authName

  return (
    <div className="dashboard">
      <p>&nbsp;</p>
      <p><small>You are logged in as</small></p>
      <h1>{name}</h1>
      <Button onClick={() => history.push('/customer')}>Now Serving</Button>
      <Button type="warn" onClick={onSignout}>Sign Out</Button>
    </div>
  )
}

const SIGNIN_MUTATION = gql`
  mutation signinUser($username: String!, $password: String!) {
    signinUser(username:$username, password:$password) {
      id
      token
      name
    }
  }
`

export default graphql(SIGNIN_MUTATION, { name: 'signinUser' })(CustomerAuth)

