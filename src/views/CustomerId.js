import React, { Component } from 'react';
import { Route } from 'react-router-dom'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

import CustomerIdShop from './CustomerIdShop'
import CustomerIdCart from './CustomerIdCart'
import CustomerProfile from './CustomerProfile'
import Header from '../components/Header'

class CustomerIdPage extends Component {
  render() {
    const {match, queryCustomer, allSellables} = this.props

    if (queryCustomer.error) {
      return <Header
        title="Error!"
        subtitle="Try refreshing the page."
      />
    }

    if (queryCustomer.loading) {
      return <Header title="Loading customer..."/>
    }

    return (
      <React.Fragment>
        <Header title={`Customer ${queryCustomer.Customer.reservation.number}`}/>
        <Route exact path={match.url}
          render={ props => (
            <CustomerProfile {...props} queryCustomer={queryCustomer} />
          )} />
        <Route path={`${match.url}/shop`}
          render={ props => (
            <CustomerIdShop {...props}
              Customer={queryCustomer}
              allSellables={allSellables}
              queryCustomer={queryCustomer} />
          )} />
        <Route path={`${match.url}/cart`}
          render={ props => (
            <CustomerIdCart {...props}
              queryCustomer={queryCustomer}
            />
          )} />
      </React.Fragment>
    )
  }
}

export const CUSTOMER_QUERY = gql`
  query getCustomer($customerId:ID!){
    Customer(id:$customerId) {
      id
      name
      reservation {
        id
        number
      }
      sales {
        id
        quantity
        createdAt
        completedAt
        tender {
          id
          name
        }
        cashier {
          id
          name
        }
        sellable {
          id
          name
          price
        }
      }
    }
  }
`

export default graphql(CUSTOMER_QUERY, {
  name: 'queryCustomer',
  options: ({match}) => {
    return { variables: { customerId: match.params.customerId }}
  }
})(CustomerIdPage)
