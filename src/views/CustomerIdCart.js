import React, { Component } from 'react';
import { Route } from 'react-router-dom'

import SalesList from '../components/SalesList'
import SalesEdit from '../components/SalesEdit'
import Header from '../components/Header'

class CustomerIdCartPage extends Component {
  render() {
    const {match, queryCustomer} = this.props

    if (queryCustomer.error) {
      return <Header
        title="Error!"
        subtitle="Try refreshing the page."
      />
    }

    if (queryCustomer.loading) {
      return <Header title="Loading customer..."/>
    }

    return (
      <React.Fragment>
        <Route exact path={match.url} render={ props => (
          <SalesList {...props}
            queryCustomer={queryCustomer}
          />
        )}/>
        <Route exact path={`${match.url}/:saleId`}
          render={ props => (
            <SalesEdit {...props}
              queryCustomer={queryCustomer}
            />
          )}/>
      </React.Fragment>
    )
  }
}

export default CustomerIdCartPage
