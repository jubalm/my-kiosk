import React, { Component } from 'react';
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import { Route } from 'react-router-dom'

import ShopList from '../components/ShopList'
import ShopListEdit from '../components/ShopListEdit'
import FixedMenu from '../components/FixedMenu'
import Button from '../components/Button'

class CustomerIdShopPage extends Component {
  render() {
    const {history, match, Customer, allSellables} = this.props

    return (
      <React.Fragment>
        <Route exact path={match.url}
          render={ props => (
            <ShopList {...props}
              allSellables={allSellables}
              Customer={Customer} />
          )}/>
        <Route path={`${match.url}/:itemId`}
          render={ props => (
            <ShopListEdit {...props}
              allSellables={allSellables}
              Customer={Customer}
            />
          )}
        />
        <FixedMenu>
          <Button type="solid" onClick={() => history.goBack()}>
            Back <span>Cart</span>
          </Button>
        </FixedMenu>
      </React.Fragment>
    )
  }
}

const PRODUCTS_QUERY = gql`
  query {
    allSellables {
      id
      name
      price
    }
  }
`

export default graphql(PRODUCTS_QUERY, { name: 'allSellables' })(CustomerIdShopPage)
