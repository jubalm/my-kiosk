import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import { matchPath } from 'react-router-dom'
import gql from 'graphql-tag'

import { RESERVATIONS_QUERY } from './Customer'
import FixedMenu from '../components/FixedMenu'
import Button from '../components/Button'

class CustomerProfile extends Component {
  state = {
    deleting: false,
    customerName: null
  }

  _abortPrompt = () => {
    this.setState({ deleting: false })
  }

  _updateReservationName = async () => {
    const { match, mutationUpdateCustomer } = this.props
    const { customerName } = this.state

    const {params:{customerId}} = matchPath(match.path, {path: '/customer/:customerId'})

    if (!customerName) return

    await mutationUpdateCustomer({
      variables: {
        customerId: customerId,
        name: this.state.customerName
      },

      update: (store, {data: { mutationUpdateCustomer:customer }}) => {
        const data = store.readQuery({ query: RESERVATIONS_QUERY })

        data.allReservations.reduce((a, reservation) => {
          if (!reservation.customer) return a

          if (reservation.customer.id === customerId) {
            reservation.customer.name = customerName
          }

          a.push(reservation)
          return a
        }, [])

        store.writeQuery({ query: RESERVATIONS_QUERY, data })
      }
    })

    this.setState({ customerName: null })
  }

  _cancelReservation = async () => {
    const {history, match, mutationUpdateCustomer} = this.props
    const {params:{customerId}} = matchPath(match.path, {path: '/customer/:customerId'})

    await mutationUpdateCustomer({
      variables: { customerId: customerId, reservation: null },
      update: (store, {data: {updateCustomer:customer}}) => {
        const data = store.readQuery({ query: RESERVATIONS_QUERY })

        data.allReservations.reduce((a, reservation) => {
          if (!reservation.customer) return a

          if (reservation.customer.id === customer.id) {
            reservation.customer = null
          }

          a.push(reservation)
          return a
        }, [])

        store.writeQuery({ query: RESERVATIONS_QUERY, data })
      }
    })

    this.setState({ deleting: false })
    history.push('/customer')

  }

  _goToCart = () => {
    const {history, match} = this.props
    history.push(`${match.url}/cart`)
  }

  _addOrder = () => {
    const {history, match} = this.props
    history.push(`${match.url}/shop`)
  }

  _goBack = () => {
    const {history} = this.props
    history.push(`/customer`)
  }

  render() {
    const {queryCustomer: {Customer}} = this.props

    return (
      <React.Fragment>
        <div className="app-body">
          <div className="form-rename">
            <input type="text"
              defaultValue={Customer.name}
              onChange={e => this.setState({ customerName: e.target.value })}
              onBlur={this._updateReservationName}
            />
          </div>
          <Button onClick={this._goToCart}>Go to cart</Button>
          <Button onClick={this._addOrder}>+ Add new order</Button>
          <Button type="warn" onClick={() => { this.setState({ deleting: true }) }}>Cancel Reservation</Button>
        </div>
        <Confirm
          when={this.state.deleting}
          message="Really cancel reservation?"
          onProceed={this._cancelReservation}
          onAbort={this._abortPrompt}
        />
        <FixedMenu>
          <Button
            type="solid"
            onClick={this._goBack}>
            Back <span>New Customer</span>
          </Button>
        </FixedMenu>
      </React.Fragment>
    )
  }
}

const Confirm = ({ when, message, onProceed, onAbort }) => {
  if (!when) return null

  return (
    <div className="confirm">
      <p>{message}</p>
      <div>
        <button onClick={onAbort}>No</button>
        <button onClick={onProceed}>Yes</button>
      </div>
    </div>
  )
}

const UPDATECUSTOMER_MUTATION = gql`
  mutation removeReservation(
    $customerId:ID!,
    $reservation: CustomerreservationReservation,
    $name: String
  ) {
    updateCustomer(
      id: $customerId,
      reservation: $reservation,
      name: $name
    ) {
      id
    }
  }
`

export default graphql(UPDATECUSTOMER_MUTATION, { name: 'mutationUpdateCustomer' })(CustomerProfile)
